---
draft: false
date: "2017-06-19T23:00:00Z"
title: "Initial commit"
---
Hey there and thanks for stopping by! I'm Roman - once a network engineer who decided to pour some thick layer of DevOps and programmability sauce on top of the network engineering base.

I am currently with **Nokia** doing Network Automation consulting which involves node-level automation for Nokia SR OS platform as well as the higher level orchestration powered with Nokia NSP.

My geek area of interest is best described with the [tags](https://netdevops.me/tags/) this blog has. It ranges from a classic Networking domain to programmable networks powered by modern model interfaces and devops stacks.

In addition to my daily customer-facing activities I embrace the open source and taking every chance to contribute to it. You might know me as one of the authors of Nokia related plugins for projects like Netmiko, NAPALM, Ansible, etc. You can also check out my own projects - https://github.com/hellt.

My programming interests are now occupied with Go, but I did enjoy Python and am a heavy user of Ansible.

Previously held positions:

* Technical Marketing Engineer @ [Nuage Networks](https://www.nuagenetworks.net/)
* Test Automation Engineer @ [Nuage Networks](https://www.nuagenetworks.net/)
* New Product Introduction engineer @ [Nokia](https://nokia.com)

### What is this blog about?
This blog will be my diary on a journey to embrace

- Scripting and tooling (Network Automation, Config Management, Test Automation, CI/CD)
- Model driven network interfaces (NETCONF, gNMI, YANG)
- Virtualization, containerization and orchestration
- Networking in the public cloud (aws, gcp, azure)

### How to reach me
I am quite active on [Twitter](https://twitter.com/ntdvps) and [LinkedIn](https://www.linkedin.com/in/rdodin/). You can also drop me an email `dodin [d0t] roman {at} gmail.com`