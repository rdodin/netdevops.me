---
date: 2017-06-25T12:00:00Z
comment_id: loom
keywords:
- screencast
- loom
tags:
- loom
title: Screencasting for teams with Loom
---

Just about a week ago I struggled looking for a quick way to record a screencast for my team members on Windows. When it comes to OS X my tool of choice is QuickTime all the time: it is free, built-in, and simple to use. But win7 lacks a similar built-in replacement, so I ended up googling _"screencast on windows free"_.


Can't say that I spent the whole day installing and trying different alternatives, but there was no obvious winner there. In the end I used [OBS](https://obsproject.com/), which is a free but pro-tool for streamers and game players. Lots of tuning options, capture settings, scene selectors. Did I need them all? Obviously I did not, but other tools were unusable in their free editions.


And today my twitter feed introduced me to [Loom](https://www.useloom.com/?ref=209582) which seems a nice and modern way to record screencasts when working in teams.

<!--more-->

Loom tries to fix three problems of screencasting software:

- **multiplatform**: Loom shipped as a Chrome extension, if you are using this browser you are covered on Linux/Win/Mac
- **ease of use**: Provides only simple controls and key mappings to make recording easy and lightweight
- **dev teams features**: Loom has a nice commenting system tied to timestamps (see video below). Timestamps also allow an author to create a table of contents for his video. Besides, Loom claims lots of integrations available now. I tried GitLab integration and its good! Of course GitHub is there, GMail, JIRA, Asana and many others.

<iframe width="630" height="394" src="https://www.useloom.com/embed/04e86800dc9011e682ec0711bb41ca23" frameborder="0" allowfullscreen></iframe>

## GitLab integration
Take a look how nicely Loom integrates into my favorite dev platform -- GitLab:
<iframe width="630" height="394" src="https://www.useloom.com/embed/b17f9c186bd7440aacc07e59ac6cdc0d" frameborder="0" allowfullscreen></iframe>

## Pricing?
Loom is free, but limits you to _10 min_ videos and your videos will only last for _7 days_. Good thing is that Loom breaks down these restrictions once you invite two newcomers. So lets help each other out and register accounts via referral links. Mine is https://www.useloom.com/?ref=209582
